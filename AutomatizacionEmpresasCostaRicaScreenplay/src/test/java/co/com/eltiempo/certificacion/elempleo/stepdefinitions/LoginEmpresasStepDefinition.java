package co.com.eltiempo.certificacion.metro.stepdefinitions;

import io.cucumber.java.Before;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import metro.exceptions.GeneralExceptions;
import metro.questions.ElementoVisible;
import metro.tasks.IniciarSesion;
import metro.userinterfaces.HomePage;
import metro.util.TraerDato;
import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.abilities.BrowseTheWeb;
import net.serenitybdd.screenplay.actions.Click;
import net.serenitybdd.screenplay.actions.Open;
import net.thucydides.core.annotations.Managed;
import org.openqa.selenium.WebDriver;

import java.io.IOException;
import java.security.GeneralSecurityException;

import static metro.userinterfaces.HomePage.BUSCO_EMPLEO_OPCION;
import static metro.userinterfaces.IniciarSesion.BOTON_CERRAR_SESION;
import static metro.userinterfaces.OpcionesPage.MENU_USUARIO;
import static metro.util.ConstantesExceptions.ERROR_INICIO_SESION;
import static metro.util.ConstantesGenerales.FILA;
import static metro.util.ConstantesGenerales.HOJA;
import static net.serenitybdd.screenplay.GivenWhenThen.seeThat;

public class LoginEmpresasStepDefinition {


	@Managed(driver = "chrome")
	private WebDriver herBrowser;
	private final Actor actor= Actor.named("Esteban");
	private final HomePage homepage = new HomePage();

	public LoginEmpresasStepDefinition() throws GeneralSecurityException, IOException {
	}

	@Before
	public void setUp(){
		actor.can(BrowseTheWeb.with(herBrowser));
	}


	@Given("^que estoy en la pagina inicial de elempleo$")
	public void queEstoyEnLaPaginaInicialDeElempleo() {
		actor.attemptsTo(
				Open.browserOn(homepage),
				Click.on(BUSCO_EMPLEO_OPCION)
		);

	}

	@When("^ingreso mis datos (.*) (.*)$")
	public void ingresoMisDatos(String fila, String hoja) throws IOException {
		actor.remember(FILA,fila);
		actor.remember(HOJA,hoja);
		actor.attemptsTo(
				IniciarSesion.enPersonas(TraerDato.delExcel("Usuario",actor),TraerDato.delExcel("Contraseņa",actor))
		);
	}

	@Then("^puedo ver las diferentes opciones$")
	public void puedoVerLasOpciones() {
		actor.should(
			seeThat(ElementoVisible.enPantalla(MENU_USUARIO)).orComplainWith(GeneralExceptions.class,ERROR_INICIO_SESION)
		);
	}


}
