package metro.util;

public class ConstantesExceptions {
    public static final String ERROR_DATOS_INCORRECTOS = "El resultado no concuerda con el resultado esperado";
    public static final String ERROR_INICIO_SESION = "Revice que las credenciales ingresadas sean correctas";
}
