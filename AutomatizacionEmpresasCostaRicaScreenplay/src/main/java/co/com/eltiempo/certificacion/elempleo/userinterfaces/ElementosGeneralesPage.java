package metro.userinterfaces;

import net.serenitybdd.screenplay.targets.Target;

public class ElementosGeneralesPage {
    public static final Target BTN_ACEPTAR_COOKIES = Target.the("Ventana que pide aceptar las cookies")
            .locatedBy("//a[contains(@class, 'AcceptPolicy')]");
}
