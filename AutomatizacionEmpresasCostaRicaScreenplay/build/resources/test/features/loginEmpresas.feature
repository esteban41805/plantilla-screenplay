# encoding: iso-8859-1
Feature: Login Exitoso
  yo como usuario de elempleo
  deseo poder realizar inicio de sesion en el aplicativo
  para poder acceder a las diferentes opciones

  @COL @rutaCritica
  Scenario Outline: login exitoso
    Given que estoy en la pagina inicial de elempleo
    When ingreso mis datos <fila> <hoja>
    Then puedo ver las diferentes opciones

    Examples:
      | fila | hoja  |
      | 1    | Login |