package metro.util;

import net.serenitybdd.screenplay.Actor;
import org.apache.poi.xssf.usermodel.XSSFCell;
import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.Objects;

import static metro.util.ConstantesGenerales.FILA;
import static metro.util.ConstantesGenerales.HOJA;

public class TraerDato {
    public static String delExcel(String nombre, Actor actor) throws IOException {

        String filePath="UAT";
        String sheetName= actor.recall(HOJA);
        int cellNumber = Integer.parseInt(actor.recall(FILA));

        switch (filePath){
            case "PROD" : filePath = "";
            case "QA" : filePath = "";
            case "UAT" : filePath = "C:\\Users\\usuario\\Desktop\\Elempleo\\AutomatizacionEmpresasCostaRicaScreenplay\\src\\main\\java\\metro\\archivos\\Empresas CO.xlsx";
        }
        File file=new File(filePath);

        FileInputStream inputStream = null;

        inputStream = new FileInputStream(file);

        XSSFWorkbook newbook = null;
        newbook = new XSSFWorkbook(inputStream);

        XSSFSheet sheet=newbook.getSheet(sheetName);
        XSSFRow row=sheet.getRow(0);


        XSSFCell cell=row.getCell(0);
        cell.getStringCellValue();

        int contador=0;

        while (!Objects.equals(cell.getStringCellValue(), "NULL")) {
            cell=row.getCell((contador));
            contador = contador + 1;

        }
        contador = contador-2;
        int posicion = 0;

        for (int i=0; i<=(contador+1);i++){

            if (Objects.equals(nombre,cell.getStringCellValue())){
                cell=row.getCell(i-1);
                posicion = i-1;
                break;
            }
            cell=row.getCell((i));
        }

        //---------------------------------------------

        row = sheet.getRow(cellNumber);

        cell = row.getCell(posicion);
        cell.getStringCellValue();

        return cell.getStringCellValue();
    }
}
