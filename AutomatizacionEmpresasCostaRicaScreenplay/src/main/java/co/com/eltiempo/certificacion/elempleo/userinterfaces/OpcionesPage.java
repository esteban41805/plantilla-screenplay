package metro.userinterfaces;

import net.serenitybdd.screenplay.targets.Target;

public class OpcionesPage {
    public static final Target MENU_USUARIO = Target.the("Opción del menu de mi usuario")
            .locatedBy("//li[contains(@class, 'user-menu')]/a");
}
