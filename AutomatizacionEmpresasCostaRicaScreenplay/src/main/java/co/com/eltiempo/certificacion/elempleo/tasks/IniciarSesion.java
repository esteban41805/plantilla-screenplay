package metro.tasks;

import metro.util.TraerDato;
import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.Task;
import net.serenitybdd.screenplay.actions.Click;
import net.serenitybdd.screenplay.actions.SendKeys;
import org.openqa.selenium.Keys;

import static metro.userinterfaces.IniciarSesion.*;
import static net.serenitybdd.screenplay.Tasks.instrumented;

public class IniciarSesion implements Task {

    private final String usuario;
    private final String contrasena;

    public IniciarSesion(String usuario, String contrasena) {
        this.usuario = usuario;
        this.contrasena = contrasena;
    }

    @Override
    public <T extends Actor> void performAs(T actor) {

        actor.attemptsTo(
                Click.on(BUTTON_INICIAR_SESION),
                SendKeys.of(usuario).into(CORREO),
                SendKeys.of(contrasena).into(CONTRASENA).thenHit(Keys.ENTER)
        );
        if (CERRAR_SESION_OTRO_DISPOSITIVO.resolveFor(actor).isVisible()){
            actor.attemptsTo(
                    Click.on(CERRAR_SESION_OTRO_DISPOSITIVO)
            );
        }
    }

    public static IniciarSesion enPersonas(String usuario,String contrasena){
        return instrumented(IniciarSesion.class,usuario,contrasena);
    }
}
