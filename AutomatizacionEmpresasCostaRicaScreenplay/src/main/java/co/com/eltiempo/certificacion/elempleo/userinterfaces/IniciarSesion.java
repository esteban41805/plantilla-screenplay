package metro.userinterfaces;

import net.serenitybdd.screenplay.targets.Target;

public class IniciarSesion {
    public static final Target BUTTON_INICIAR_SESION = Target.the("Botón para ir al formulario de inicio de sesión")
            .locatedBy("//div/a[contains(@href, 'iniciar-sesion')]");
    public static final  Target CORREO = Target.the("Campo para iniciar sesión")
            .locatedBy("//*[@name='EmailField' or @name='Email']");
    public static final Target CONTRASENA = Target.the("Campo para ingresar la contraseña")
            .locatedBy("//*[@name='PasswordField' or @name='Password']");
    public static final Target BOTON_CERRAR_SESION = Target.the("Boton para cerrar la sesión")
            .locatedBy("//a[@class='log-off js-logOff']");
    public static final Target CERRAR_SESION_OTRO_DISPOSITIVO = Target.the("Cerrar la sesión en otro dispositivo")
            .locatedBy("//div[@class='modal fade js-recover-password-success-modal   in']//div[@class='col-xs-6 col-md-4 col-md-push-2']/a[@class='acept btn btn-block btn-primary']");

}
