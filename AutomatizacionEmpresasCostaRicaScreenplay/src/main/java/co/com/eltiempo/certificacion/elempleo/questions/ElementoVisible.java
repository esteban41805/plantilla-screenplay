package metro.questions;

import metro.interactions.EsperarelElemento;
import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.Question;
import net.serenitybdd.screenplay.targets.Target;


public class ElementoVisible implements Question<Boolean> {

    private final Target target;

    public ElementoVisible(Target target) {
        this.target = target;
    }

    @Override
    public Boolean answeredBy(Actor actor) {
        actor.attemptsTo(
                EsperarelElemento.unMomento(target,10)
        );
        return target.resolveFor(actor).isVisible();
    }

    public static ElementoVisible enPantalla(Target target){
        return new ElementoVisible(target);
    }
}
