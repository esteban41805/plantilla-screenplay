package metro.interactions;

import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.Interaction;
import net.serenitybdd.screenplay.targets.Target;
import net.serenitybdd.screenplay.waits.Wait;
import net.serenitybdd.screenplay.waits.WaitUntil;

import static net.serenitybdd.screenplay.Tasks.instrumented;
import static net.serenitybdd.screenplay.matchers.WebElementStateMatchers.isVisible;

public class EsperarelElemento implements Interaction {
    private final Target target;
    private final int time;

    public EsperarelElemento(Target target, int time) {
        this.target = target;
        this.time = time;
    }

    @Override
    public <T extends Actor> void performAs(T actor) {
        actor.attemptsTo(

                WaitUntil.the(target, isVisible()).forNoMoreThan(time).seconds()
        );
    }

    public static EsperarelElemento unMomento(Target target,int time){
        return instrumented(EsperarelElemento.class,target,time);
    }
}
