package metro.interactions;

import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.Interaction;
import net.serenitybdd.screenplay.actions.Click;

import static metro.userinterfaces.ElementosGeneralesPage.BTN_ACEPTAR_COOKIES;
import static net.serenitybdd.screenplay.Tasks.instrumented;

public class AceptarCookies implements Interaction {

    @Override
    public <T extends Actor> void performAs(T actor) {
        if (BTN_ACEPTAR_COOKIES.resolveFor(actor).isVisible()) {
            actor.attemptsTo(
                    Click.on(BTN_ACEPTAR_COOKIES)
            );
        }

    }

    public AceptarCookies boton(){
        return instrumented(AceptarCookies.class);
    }
}
