<html class="ee-html" lang="es"><head>

    <!-- Google Tag Manager -->
    <script async="" src="https://tags.bkrtx.com/js/bk-coretag.js"></script><script type="text/javascript" async="" src="https://www.google-analytics.com/analytics.js"></script><script async="" src="https://www.googletagmanager.com/gtm.js?id=GTM-K9L3D4J"></script><script async="" src="https://www.googletagmanager.com/gtm.js?id=GTM-K9L3D4J"></script><script>
        (function(w, d, s, l, i) {
            w[l] = w[l] || [];
            w[l].push({
                'gtm.start':
                    new Date().getTime(),
                event: 'gtm.js'
            });
            var f = d.getElementsByTagName(s)[0],
                j = d.createElement(s),
                dl = l != 'dataLayer' ? '&l=' + l : '';
            j.async = true;
            j.src =
                'https://www.googletagmanager.com/gtm.js?id=' + i + dl;
            f.parentNode.insertBefore(j, f);
        })(window, document, 'script', 'dataLayer', 'GTM-K9L3D4J');
    </script>
    <!-- End Google Tag Manager -->

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="format-detection" content="telephone=no">
    <link rel="icon" type="image/png" href="/resources/companies/Content/dist/images/Content/dist/images/favicon.png">
    <link rel="shortcut icon" type="image/png" href="/resources/companies/Content/dist/images/Content/dist/images/favicon.png">

    
    <link href="/resources/companies/Content/dist/css/vendor/vendor.min.css" rel="stylesheet">
    <link href="/resources/companies/Content/dist/css/layoutSimple.min.css" rel="stylesheet">
    

    <script type="text/javascript">
        window.cultureName = 'es-CO';
    </script>

    
    

<!-- Google Tag Manager -->
<script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
})(window,document,'script','dataLayer','GTM-K9L3D4J');</script>
<!-- End Google Tag Manager -->



<title>Iniciar Sesión  - Elempleo.com</title>
<meta name="title" content="Iniciar Sesión  - Elempleo.com">
<meta name="description" content="Inicia sesión para acceder a más de 60.000 mil ofertas de empleo para ti">

<meta property="og:type" content="website">
<meta property="og:url" content="https://uatazure.elempleo.com/co/iniciar-sesion">
<meta property="og:title" content="Iniciar Sesión  - Elempleo.com">
<meta property="og:description" content="Inicia sesión para acceder a más de 60.000 mil ofertas de empleo para ti">
<meta property="og:image" content="http://uatazure.elempleo.com/resources/companies/Content/dist/images/layout/es-CO_ee_logo_seo.png">
<meta property="og:site_name" content="elempleo.com">
<meta property="fb:app_id" content="660708945">


<meta property="twitter:card" content="summary">
<meta property="twitter:site" content="@elempleocom">
<meta property="twitter:title" content="Iniciar Sesión  - Elempleo.com">
<meta property="twitter:description" content="Inicia sesión para acceder a más de 60.000 mil ofertas de empleo para ti">
<meta property="twitter:image" content="http://uatazure.elempleo.com/resources/companies/Content/dist/images/layout/es-CO_ee_logo_seo.png">
<meta property="twitter:url" content="https://uatazure.elempleo.com/co/iniciar-sesion">


<link rel="canonical" href="https://uatazure.elempleo.com/co/iniciar-sesion">


    
    <script src="//assets.adobedtm.com/41d3b477534338831f14b02d7af1cd359b73904f/satelliteLib-08d2699371d5edc2fd778cf562e1060c657326bc-staging.js"></script>

</head>
<body class="ee-body Account-container Account-Login-container modal-open" style="padding-right: 17px;">
<!-- Google Tag Manager (noscript) -->
<noscript>
    <iframe src="https://www.googletagmanager.com/ns.html?id=GTM-K9L3D4J"
            height="0" width="0" style="display: none; visibility: hidden"></iframe>
</noscript>
<!-- End Google Tag Manager (noscript) -->

<script id="hbtpl-notifications" type="text/x-handlebars-template">
        <div class="alert new-alert new-alert-{{ alertType }}" role="alert">
            <span class="new-alert-icon"><i class="iee icon-{{ iconType }}" aria-hidden="true"></i></span>
            <div class="new-alert-paragraph">
                <h3>
                    {{{ title }}}
                </h3>
                {{{ copy }}}
            </div>
            {{#if closeBtn}}
            <button class="close" type="button" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true"> × </span>
            </button>
            {{/if}}
        </div>
    </script>

<div>

<div class="modal fade js-recover-password-success-modal" data-backdrop="static" data-keyboard="false" id="personOrCompanyModal" role="dialog" aria-labelledby="myRecoverPassword" tabindex="-1">

    <div class="modal-dialog modal-sm" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true"><i class="fa fa-times-circle"></i></span></button>
                <h3 class="modal-title modal-title-align">
                    Este correo está registrado en dos cuentas.
                </h3>
            </div>

            <div class="modal-body">
                <strong>Iniciar sesión como:</strong>
            </div>

            <div class="modal-footer">
                <div class="row">
                    <div class="col-xs-6">
                        <button class="btn btn-block btn-secondary btn-go-contact" id="btnCandidate" data-url="">Candidato</button>
                    </div>
                    <div class="col-xs-6">
                        <button class="btn btn-block btn-secondary btn-go-contact" id="btnCompany" data-url="">Empresa</button>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>    
</div>
<div class="text-center ee-global-spinner-wrapper js-spinner hidden">
    <i class="fa fa-spinner fa-pulse fa-3x ee-global-spinner"></i>
</div>


<div class="alerts">
    <div class="hide alert alert-warning browser-update js-browser" role="alert">
        <div class="container">
            <p class="alert-content ee-mod">Por favor, actualiza tu navegador para tener una mejor experiencia, prueba con <a href="https://www.google.es/chrome/browser/desktop/" target="_blank"> Chrome</a><span>,</span> <a href="https://www.microsoft.com/es-co/download/internet-explorer-11-for-windows-7-details.aspx" target="_blank">Internet explorer</a>.</p>
        </div>
    </div>
</div>

<header class="header-simple">
  <div class="container">
    <div class="row">
      <div class="col-xs-12">
        <a class="ee-logotype" href="https://uatazure.elempleo.com/co/" title="El empleo">
          <img class="img-responsive ee-image hidden-xs" src="/resources/companies/Content/dist/images/icons/custom/elempleo_logo.svg" alt="el empleo">
            
          <img class="img-responsive ee-image visible-xs-block" src="/resources/companies/Content/dist/images/resources/es-CO_ee_logo_white_transparent.svg" alt="el empleo">
        </a>
      </div>
    </div>
  </div>
</header>



<div class="container">


    <div class="container breadcrumb-container">

    <ol class="breadcrumb" itemscope="" itemtype="http://schema.org/BreadcrumbList">
                <li itemprop="itemListElement" itemscope="" itemtype="http://schema.org/ListItem">
                    <a href="/co/" itemprop="item" title="Inicio">
                        <span itemprop="name">Inicio</span>
                    </a>
                    <meta itemprop="position" content="1">
                </li>
                <li class="active">
                    <span>Inicia sesión</span>
                    <meta content="2">
                </li>
    </ol>

</div>
</div>

<input class="js-conf-global-data" type="hidden" data-dmp-enable="True">
<div class="container notifications  js-notifications"></div>


<div class="btn-scrollup hidden-md hidden-lg"><a href="#top"><i class="fa fa-angle-up"></i></a> </div>







<div class="container cont-first">

    <div class="row">
        <div class="col-lg-6 col-md-6 ee_login_container">
            <h1 class="myresume-title">
                Iniciar sesión
            </h1>
            <label>Inicia sesión con tu usuario y contraseña de elempleo.com</label>
            <fieldset>
<form action="/co/Account/_Login" class="js-login-form" id="demo-form" method="post" novalidate="novalidate">                    <input type="hidden" name="g-recaptcha-response" id="g-recaptcha-response" value="">
                    <input type="hidden" id="confirmCloseOtherSessionsUrl" value="/co/Account/_ConfirmCloseOtherSessions">
<input name="__RequestVerificationToken" type="hidden" value="ejWZF4PHlCbsT0ZGiP9bMcMEi3KSeSqvzu0UR9B3Nv3kAB8nMMZyjwDxHjF9lzNCCncH7NVqJDKyXw8lUhKsEGI6ePU1"><input id="PreviousUrl" name="PreviousUrl" type="hidden" value=""><input id="IsLoggedValidateUrl" name="IsLoggedValidateUrl" type="hidden" value="/co/Account/IsLoggued">
<div class="form-group has-success">
    
<label class="control-label " for="text-22141932">Correo electrónico</label>
    
    <div class="">

        <input aria-describedby="helpBlock-22141932 text-22141932-error" class="form-control input-lg encrypt-email" data-title="" data-val="true" data-val-length="Ingresa entre 7 y 75 caracteres máximo" data-val-length-max="75" data-val-minlength="Ingresa entre 7 y 75 caracteres máximo" data-val-minlength-min="7" data-val-regex="Ingresa un correo válido." data-val-regex-pattern="^(?:[a-zA-Z0-9_'^&amp;/+-])+(?:\.(?:[a-zA-Z0-9_'^&amp;/+-])+)*@(?:(?:\[?(?:(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?))\.){3}(?:(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\]?)|(?:[a-zA-Z0-9-_]+\.)+(?:[a-zA-Z]){2,}\.?)$" data-val-required="El campo &quot;Dirección de e-mail&quot; es requerido" id="text-22141932" name="EmailField" placeholder="ejemplo@elempleo.com" required="true" type="text" value="" aria-required="true" aria-invalid="false">
        <span class="help-block field-validation-valid" data-valmsg-for="EmailField" data-valmsg-replace="true" id="helpBlock-22141932"></span>

    </div>
</div>

<div class="form-group has-success">

  <label class="control-label " for="password-56619047">Contraseña</label>

  
  <div class="">

    <div class="password-wrapper"><input aria-describedby="helpBlock-56619047 password-56619047-error" class="form-control input-lg" data-title="" data-val="true" data-val-length="Ingresa entre 6 y 15 caracteres máximo" data-val-length-max="15" data-val-minlength="Ingresa entre 6 y 15 caracteres máximo" data-val-minlength-min="6" data-val-required="El campo contraseña es requerido" id="password-56619047" name="PasswordField" placeholder="Contraseña" required="true" type="password" value="" aria-required="true" aria-invalid="false"><span class="password-revealer js-password-revealer active"><i class="fa fa-eye-slash"></i></span></div>

    <span class="help-block field-validation-valid" data-valmsg-for="PasswordField" data-valmsg-replace="true" id="helpBlock-56619047"></span>

  </div>
</div>

                    <div class="form-group">


                        <button class="btn btn-primary btn-block btn-lg js-googleTagManager js-encrypt-email" type="submit">
                            Inicia sesión
                        </button>
                    </div>
                    <div class="row link-container">
                        <div class="col-sm-6">
                            <a class="btn btn-link btn-block jsRecoverPassword js-googleTagManager btn-lg" href="/co/recuperar-contrasena">
                                Olvidé mi contraseña »
                            </a>
                        </div>
                        <div class="col-sm-6">
                            <a class="btn btn-link btn-block btn-lg login-change-email-btn js-googleTagManager" href="https://uatazure.elempleo.com/co/cambio-cuenta-correo">
                                ¿Cambiar tu correo? »
                            </a>
                        </div>
                    </div>
</form>            </fieldset>
        </div>
        <div class="col-lg-6 col-md-6 ee_signin">
            <div class="ee_not-login_container">
                <div class="ee_not-login_inner_container">
                    <h1 class="login-title">¿Aún no estás registrado?</h1>
                    <span>Aplica a miles de ofertas de empleo o encuentra los mejores candidatos para tu empresa </span>
                    <div>
                        <a class="btn-singin register-resume js-googleTagManager" href="https://uatazure.elempleo.com/co/registro-hoja-de-vida/" role="button">Registrar hoja de vida</a>
                        <a class="btn-singin register-companie js-googleTagManager" href="/co/registrar-empresa" role="button">Registrar empresa</a>
                    </div>
                </div>
            </div>

        </div>
    </div>
</div>

<div class="g-recaptcha" type="submit" data-sitekey="6LcNBXMUAAAAAM3lU3vX_4GguXp1PAuFv1BB6pDu" data-callback="verifyReCaptchaCallback" data-hide="True" data-size="invisible">
</div>




<footer class="footer">
    <div class="site-legal">
        <div class="container">
            <div class="row">
                <div class="col-xs-12 col-sm-3">
                    <img class="ee-logotype img-responsive" src="/resources/companies/Content/dist/images/resources/es-CO_ee_logo_white_transparent.svg" alt="el empleo" height="45" width="213">
                </div>

                <div class="col-xs-12 col-sm-9">
                    <div class="clearfix">

                        <div class="pull-left">
                            <img class="img-responsive" src="/resources/companies/Content/dist/images/resources/CEET_blanco.png" alt="Logo ElTiempo" title="El Tiempo, Casa Editorial" height="24" width="93">
                        </div>


                        <div class="pull-right">
                            <img class="img-responsive pull-right" src="/resources/companies/Content/dist/images/resources/logo_sic.png" alt="Logo SIC" title="Superintendencia de Industria y Comercio" height="30" width="118">
                        </div>
                    </div>


                    <p class="ee-copyright">
                        COPYRIGHT © 2022 Leadearsearch S.A.S Prohibida su reproducción total o parcial, así como su traducción a cualquier idioma sin autorización escrita de su titular. elempleo.com es un producto de Leadearsearch S.A.S. Nit. 8300651578.
                        <a class="ee-highlight-link" href="/co/aviso-privacidad" target="_blank" rel="nofollow">
                            <strong>
                                Aviso de privacidad
                            </strong>
                        </a>
                    </p>
                    <p class="ee-term-and-conditions">
                        <a class="ee-highlight-link" href="/co/terminos-condiciones" target="_blank" rel="nofollow">
                            <i class="fa fa-file-text fa-fw text-muted"></i>
                            <strong>
                                Términos y condiciones
                            </strong>
                        </a>
                    </p>
                </div>
            </div>
        </div>
    </div>
</footer>
<script src="/resources/companies/Scripts/dist/js/layout.main.vendor.min.js"></script>
<script src="/resources/companies/Scripts/dist/js/gta_cross/gta_cross.index.min.js"></script>

    <script src="/resources/companies/Scripts/dist/js/Areas/Persons/Crypto.min.js"></script> 
    <script src="/resources/companies/Scripts/dist/js/login/login.index.min.js"></script><div class="modal fade js-recover-password-success-modal  " data-backdrop="static" data-keyboard="false" id="378655" role="dialog" aria-labelledby="myRecoverPassword" tabindex="-1">

    <div class="modal-dialog modal-md" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true"><i class="fa fa-times-circle"></i></span></button>
                <h4 class="modal-title modal-title-align">
                    undefined
                </h4>
            </div>

            <div class="modal-body modal-content-center">
                <p>undefined</p>
            </div>

            <div class="modal-footer">
                <div class="row acept-cancel-modal">
                    <div class="col-xs-6 col-md-4 col-md-push-2">
                        <a data-dismiss="modal" class="cancel">Cancelar »</a>
                    </div>
                    <div class="col-xs-6 col-md-4 col-md-push-2">
                        <a class="acept btn btn-block btn-primary">Aceptar</a>
                    </div>
                </div>
                <div class="row acept-modal">
                    <div class="col-xs-12 col-md-4 col-md-push-4">
                        <a class="acept btn btn-block btn-primary">Aceptar</a>
                    </div>
                </div>

            </div>
        </div>
    </div>
</div><div class="modal fade js-recover-password-success-modal   in" data-backdrop="static" data-keyboard="false" id="316722" role="dialog" aria-labelledby="myRecoverPassword" tabindex="-1" style="display: block; padding-right: 17px;">

    <div class="modal-dialog modal-md" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true"><i class="fa fa-times-circle"></i></span></button>
                <h4 class="modal-title modal-title-align">Alerta</h4>
            </div>

            <div class="modal-body modal-content-center">
                <p>Es posible que exista una sesión abierta con este usuario en otro equipo o navegador. ¿Desea cerrar la sesión duplicada?</p>
            </div>

            <div class="modal-footer">
                <div class="row acept-cancel-modal">
                    <div class="col-xs-6 col-md-4 col-md-push-2">
                        <a data-dismiss="modal" class="cancel">Cancelar »</a>
                    </div>
                    <div class="col-xs-6 col-md-4 col-md-push-2">
                        <a class="acept btn btn-block btn-primary">Aceptar</a>
                    </div>
                </div>
                <div class="row acept-modal" style="display: none;">
                    <div class="col-xs-12 col-md-4 col-md-push-4">
                        <a class="acept btn btn-block btn-primary">Aceptar</a>
                    </div>
                </div>

            </div>
        </div>
    </div>
</div>
    <script src="/resources/companies/../Scripts/dist/js/Areas/Persons/EncryptEmail.min.js"></script>


<script type="text/javascript">_satellite.pageBottom();</script><!-- Begin BlueKai Tag -->
<script type="text/javascript">
  //md5 and sha256 provided by https://github.com/jbt/js-crypto
  var bket_md5 = function(){for(var m=[],l=0;64>l;)m[l]=0|4294967296*Math.abs(Math.sin(++l));return function(c){var e,g,f,a,h=[];c=unescape(encodeURI(c));for(var b=c.length,k=[e=1732584193,g=-271733879,~e,~g],d=0;d<=b;)h[d>>2]|=(c.charCodeAt(d)||128)<<8*(d++%4);h[c=16*(b+8>>6)+14]=8*b;for(d=0;d<c;d+=16){b=k;for(a=0;64>a;)b=[f=b[3],(e=b[1]|0)+((f=b[0]+[e&(g=b[2])|~e&f,f&e|~f&g,e^g^f,g^(e|~f)][b=a>>4]+(m[a]+(h[[a,5*a+1,3*a+5,7*a][b]%16+d]|0)))<<(b=[7,12,17,22,5,9,14,20,4,11,16,23,6,10,15,21][4*b+a++%4])|f>>>32-b),e,g];for(a=4;a;)k[--a]=k[a]+b[a]}for(c="";32>a;)c+=(k[a>>3]>>4*(1^a++&7)&15).toString(16);return c}}();
  var bket_sha256 = function(){function e(a,b){return a>>>b|a<<32-b}for(var b=1,a,m=[],n=[];18>++b;)for(a=b*b;312>a;a+=b)m[a]=1;b=1;for(a=0;313>b;)m[++b]||(n[a]=Math.pow(b,.5)%1*4294967296|0,m[a++]=Math.pow(b,1/3)%1*4294967296|0);return function(g){for(var l=n.slice(b=0),c=unescape(encodeURI(g)),h=[],d=c.length,k=[],f,p;b<d;)k[b>>2]|=(c.charCodeAt(b)&255)<<8*(3-b++%4);d*=8;k[d>>5]|=128<<24-d%32;k[p=d+64>>5|15]=d;for(b=0;b<p;b+=16){for(c=l.slice(a=0,8);64>a;c[4]+=f)h[a]=16>a?k[a+b]:(e(f=h[a-2],17)^e(f,19)^f>>>10)+(h[a-7]|0)+(e(f=h[a-15],7)^e(f,18)^f>>>3)+(h[a-16]|0),c.unshift((f=(c.pop()+(e(g=c[4],6)^e(g,11)^e(g,25))+((g&c[5]^~g&c[6])+m[a])|0)+(h[a++]|0))+(e(d=c[0],2)^e(d,13)^e(d,22))+(d&c[1]^c[1]&c[2]^c[2]&d));for(a=8;a--;)l[a]=c[a]+l[a]}for(c="";63>a;)c+=(l[++a>>3]>>4*(7-a%8)&15).toString(16);return c}}();

  function bket_hash(str) {
    bket_hash_time(str, 24 * 365);
  }

  function bket_hash_time(str, exphours) {
    if (str != "") {
      str = str.toLowerCase();
      var email = str.substring(0, str.indexOf("@"));
      var hash = bket_md5(email);
      hash = str + hash;
      hash = bket_sha256(hash);

      if (bket_get_cookie("bket_i") != hash) {
        bket_set_cookie("bket_i", hash, exphours);
        bket_delete_cookie("bket_e");
        bket_check_hash();
      }
    }
  }

  function bket_check_hash() {
    var id = bket_get_cookie("bket_i");
    if (id != "") {
      var exp = bket_get_cookie("bket_e");
      if (exp == "") {
        //Create expiration cookie valid for 6 days
        bket_set_cookie("bket_e", Math.random() * 100, 6 * 24);

        //Call bluekai tag
        bk_addPageCtx("id", id);

        bk_allow_multiple_calls = true; 
        bk_use_multiple_iframes = true;

        BKTAG.doTag(_satellite.getDataElement('BK_CeetId'), 10);
      }
    }
  }

  function bket_set_cookie(cname, cval, exphours) {
    var d = new Date();
    d.setTime(d.getTime() + (exphours * 60 * 60 * 1000));
    var expires = "expires=" + d.toUTCString();
    document.cookie = cname + "=" + cval + "; " + expires + ";path=/";
  }

  function bket_get_cookie(cname) {
    var name = cname + "=";
    var ca = document.cookie.split(';');
    for (var i = 0; i < ca.length; i++) {
      var c = ca[i];
      while (c.charAt(0) == ' ') c = c.substring(1);
      if (c.indexOf(name) == 0) return c.substring(name.length, c.length);
    }
    return "";
  }  

  function bket_delete_cookie(cname) {
    document.cookie = cname + '=;path=/;expires=Thu, 01 Jan 1970 00:00:01 GMT;';
  }

  function bket_data_collection() {
    bk_allow_multiple_calls = true; 
    bk_use_multiple_iframes = true;

    _satellite.getDataElement('BK_JobDetailVars');

    BKTAG.doTag(_satellite.getDataElement('BK_SiteId'), 10);
  }

  window.bk_async = function() {
    if (typeof(bket_callback) == "function") {
      bket_callback();
    }

    bket_check_hash();
    bket_data_collection();
  };

  (function() {
    var scripts = document.getElementsByTagName('script')[0];
    var s = document.createElement('script');
    s.async = true;
    s.src = "https://tags.bkrtx.com/js/bk-coretag.js";
    scripts.parentNode.insertBefore(s, scripts);
  }());
</script>
<!-- End BlueKai Tag -->



<script type="text/javascript" id="">(function(b){b(document).ajaxSuccess(function(a,c,d){a=google_tag_manager["GTM-K9L3D4J"].macro(6);dataLayer.push({event:"ajaxSuccess",eventCategory:"AJAX2",eventAction:d.url,eventLabel:c.responseText,url:a});console.log("ontime inside ajax")});console.log("ontime")})(jQuery);</script><iframe name="__bkframe" id="__bkframe" title="bk" src="about:blank" style="border: 0px; width: 0px; height: 0px; display: none; position: absolute; clip: rect(0px, 0px, 0px, 0px);"></iframe><iframe name="__bkframe_34070_1658525929712" id="__bkframe_34070_1658525929712" title="bk" src="https://stags.bluekai.com/site/34070?ret=html&amp;phint=__bk_t%3DIniciar%20Sesi%C3%B3n%20-%20Elempleo.com&amp;phint=__bk_k%3D&amp;phint=__bk_pr%3Dhttps%3A%2F%2Fuatazure.elempleo.com%2Fco%2Fofertas-empleo%2F&amp;phint=__bk_l%3Dhttps%3A%2F%2Fuatazure.elempleo.com%2Fco%2Finiciar-sesion&amp;phint=__bk_v%3D3.1.10&amp;limit=10&amp;r=88332813" class="__bkframe_site_34070" style="border: 0px; width: 0px; height: 0px; display: none; position: absolute; clip: rect(0px, 0px, 0px, 0px);"></iframe><div class="modal-backdrop fade in"></div><iframe name="__bkframe_34038_1658525931000" id="__bkframe_34038_1658525931000" title="bk" src="https://stags.bluekai.com/site/34038?ret=html&amp;phint=id%3Dd950cc767ad82fe7f25038af1c2268d027d7da7719e67066fcf317848eefb636&amp;phint=__bk_t%3DIniciar%20Sesi%C3%B3n%20-%20Elempleo.com&amp;phint=__bk_k%3D&amp;phint=__bk_pr%3Dhttps%3A%2F%2Fuatazure.elempleo.com%2Fco%2Fofertas-empleo%2F&amp;phint=__bk_l%3Dhttps%3A%2F%2Fuatazure.elempleo.com%2Fco%2Finiciar-sesion&amp;phint=__bk_v%3D3.1.10&amp;limit=10&amp;r=93174443" class="__bkframe_site_34038" style="border: 0px; width: 0px; height: 0px; display: none; position: absolute; clip: rect(0px, 0px, 0px, 0px);"></iframe></body></html>